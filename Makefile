run:
	./lua_modules/bin/fennel main.fnl

setup:
	echo "Make sure sqlite3 and gtk+ development libraries are installed"
	mkdir -p lua_modules
	luarocks install --tree lua_modules lgi
	luarocks install --tree lua_modules lsqlite3
	luarocks install --tree lua_modules fennel
	luarocks install --tree lua_modules luafilesystem

clean:
	rm -rf ~/.cache/indie-game-db

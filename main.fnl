;; https://github.com/Miqueas/Lua-GTK3-Examples
(global fennel (require :fennel))
(global pp (fn [x] (print (fennel.view x))))

(local version (_VERSION:match "%d+%.%d+"))

(set package.path  (.. (.. "lua_modules/share/lua/" version "/?.lua") ";"
                       (.. "lua_modules/share/lua/" version "/?.lua") ";"
                       (.. "lua_modules/share/lua/"  version "/?/init.lua") ";"
                       package.path))

(set package.cpath  (.. "lua_modules/lib/lua/" version "/?.so" ";"
                        package.cpath))

(set fennel.path (.. "/?.fnl;"
                     "./src/?.fnl;"
                     fennel.path))

(local DEFAULT_CACHE_LOCATION (.. (os.getenv "HOME" ) "/.cache/indie-game-db"))

(local db ((require :database) DEFAULT_CACHE_LOCATION))
(local gui ((require :gui) arg db))


(local sqlite3 (require :sqlite3))

;; http://keplerproject.github.io/luafilesystem/manual.html
;; http://lua.sqlite.org/index.cgi/doc/tip/doc/lsqlite3.wiki

(local db {})

(local resources-table
       ["id INTEGER PRIMARY KEY"
        "name TEXT"        
        "author TEXT"
        "licence TEXT"
        "link TEXT"
        "category TEXT"
        "datatype INTEGER"        
        "data BLOB"])

(local game-table
       ["id INTEGER PRIMARY KEY"
        "name TEXT"        
        "author TEXT"
        "link TEXT"
        ])

(local gameresources-table
       ["id INTEGER PRIMARY KEY"
        "gameid INTEGER"
        "resourceid INTEGER"])

(fn get-keys [t]
  (let [ret []]
    (each [key _ (pairs t)]
      (table.insert ret key))
    ret))

(fn concat-keys [columns prefix?]
  (local prefix (or prefix? ""))
  (var str "")
  (each [_ value (ipairs columns)]
    (if (= str "")
        (set str  (.. prefix value))
        (set str (.. str ", " prefix value))))
  str)

(fn db.initialize [self]
  (self.db:exec "DROP TABLE IF EXISTS resources;")
  (self.db:exec "DROP TABLE IF EXISTS games;")
  (self.db:exec "DROP TABLE IF EXISTS gameresources;")
  (self.db:exec "CREATE TABLE resources " (concat-keys resources-table) ";")
  (self.db:exec "CREATE TABLE games " (concat-keys game-table) ";")
  (self.db:exec "CREATE TABLE gameresources " (concat-keys gameresources-table) ";"))

(fn add-smart [db table-name keys data]
  (pp db)
  (local prep-string (.. "INSERT INTO " table-name " VALUES (" (concat-keys keys ":") ")"))
  (pp prep-string)
  (local stmt (db:prepare prep-string))
  (pp stmt)
  (each [_ row (ipairs data)]    
    (stmt:bind_names row)
    (stmt:step)
    (stmt:reset))
  (stmt:finalize))

(fn add_row [db name t]
  (add-smart db name (get-keys t) t))

(fn db.add-resource [self name author licence link category datatype data]
  (add_row self.db :resources
           {:id "NULL"
            :name (or name "")
            :author (or author "")
            :licence (or licence "")
            :link (or link "")
            :category (or category "undefined")
            :datatype (or datatype 0)
            :data (or data "e")}))

(fn db.remove-resource [self id]
  (self.db:exec (.. "DELETE FROM resources WHERE id =" id ";")))

(fn db.add-game [self name author link]
  (add_row self.db :games {:id "NULL"
                   : name  : author : link}))

(fn db.remove-game [self id]
  (self.db:exec (.. "DELETE FROM games WHERE id =" id ";")))

(fn db.add-resource-to-game [self resourceid gameid]
  (add_row self.db :gameresources {:id "NULL"
                                   : resourceid  : gameid}))

(fn db.remove-resource-to-game [self id]
  (self.db:exec (.. "DELETE FROM gameresources WHERE id =" id ";")))

(fn db.get-whole-table [self table-name]
  (let [ret []]
    (each [row _ (self.db:rows (.. "SELECT * FROM " table-name))]
      (tset ret row.id row.content))
    ret))

(fn db.get-resources [self]
  (self.db.get-whole-table :resources))

(fn db.get-games [self]
  (self.db.get-whole-table :games))

(fn db.get-game-resources []
  (pp :stub))

(local db-mt {:__index db})

(fn [directory]
  (local lfs (require :lfs))
  (local content-directory (.. directory "/contents"))
  (local database-file (.. directory "/db.sqlite3"))
  (assert directory "missing directory argument to database")
  (assert (= (type directory) :string) "directory argument should be a string")
  (if (~= (lfs.attributes directory :mode) :directory)
      (lfs.mkdir directory))
  (assert (= (lfs.attributes directory :mode) :directory) (.. "failed to make new directory: " directory))
    (if (~= (lfs.attributes content-directory :mode) :directory)
      (lfs.mkdir content-directory))
  (var first-run false)
  (if (~= (lfs.attributes database-file :mode) :file)
      (set first-run true))   
  (let [ret {:db (sqlite3.open database-file)}]    
    (setmetatable ret db-mt)
    (when first-run (ret:initialize))
    ret))

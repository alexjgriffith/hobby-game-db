
(local lgi (require :lgi))
(local gtk (lgi.require :Gtk :3.0))

;; https://github.com/Miqueas/Lua-GTK3-Examples

(local gui {})
(local app
         (gtk.Application {:application_id :com.alexjgriffith.indie-game-db}))

(var context :resources)
(var db nil)
(var container nil)
(var scroll nil)
(var dblist nil)
(var aw nil)

;; Box - vertical
;; - Box - horizontal
;;   - Button Resources
;;   - Button Games
;;   - Button Export
;;   - Button Add
;; - ScrollWindow
;;   - ListBox

(fn app.on_startup [self]
  (gtk.ApplicationWindow
   {:application self
    :default_width 400
    :default_height 400
    :border_width 10}))

(local topbar
       (gtk.Box
          {:visible true
           :orientation gtk.Orientation.HORIZONTAL
           :spacing 10
           :valign gtk.Align.START
           :halign gtk.Align.START
           }))

(local resources (gtk.Button
                  {:visible true
                   :label "Resources"
                   :valign gtk.Align.CENTER
                   :halign gtk.Align.CENTER}))

(local games (gtk.Button
              {:visible true
               :label "Games"
               :valign gtk.Align.CENTER
               :halign gtk.Align.CENTER}))

(local export (gtk.Button
               {:visible true
                :label "Export"
                :valign gtk.Align.CENTER
                :halign gtk.Align.CENTER}))

(local new (gtk.Button
              {:visible true
               :label "New"
               :valign gtk.Align.CENTER
               :halign gtk.Align.CENTER}))

(topbar:pack_start resources false true 0)
(topbar:pack_start games false true 0)
(topbar:pack_start export false true 0)
(topbar:pack_end new false true 0)

(fn display-db [db]
  (local dblist (gtk.ListBox {:visible true}))
  (local scroll (gtk.ScrolledWindow
                 {:visible true
                  :shadow_type gtk.ShadowType.NONE
                  :propagate_natural_width true
                  :propagate_natural_height true
                  1 dblist}))
  (values scroll dblist))

(fn display []
  (set (scroll dblist) (display-db db context [:name :author :link]))
  (set container
         (gtk.Box
          {:visible true
           :orientation gtk.Orientation.VERTICAL
           :spacing 10
           :valign gtk.Align.START
           :halign gtk.Align.START
           }))
  (container:pack_start topbar false true 0)
  (container:pack_start scroll false true 0))

(fn games.on_clicked [self]
  (set context :games)
  (container:remove scroll)
  (local titlebar (gtk.HeaderBar
                   {:visible true
                    :show_close_button true
                    :title "Indie Dev DB"
                    :subtitle context}))
  (set (scroll dblist) (display-db db context [:name :author :link]))
  (container:pack_start scroll false true 0)
  (aw:set_titlebar titlebar))

(fn resources.on_clicked [self]
  (set context :resources)
  (container:remove scroll)
  (local titlebar (gtk.HeaderBar
                   {:visible true
                    :show_close_button true
                    :title "Indie Dev DB"
                    :subtitle context}))
  (set (scroll dblist) (display-db db context [:name :author :link]))
  (container:pack_start scroll false true 0)
  (aw:set_titlebar titlebar))

(var data {})

(fn label-entry [name]
  (local entry (gtk.Entry {:visible true}))
  (fn entry.on_key_release_event [self]
    (tset data name self.text)
    (pp data))
  (values
   (gtk.Label {:visible true
               :label name})
   entry))

(fn new.on_clicked [self]
  (set context :new)
  (container:remove scroll)
  (local titlebar (gtk.HeaderBar
                   {:visible true
                    :show_close_button true
                    :title "Indie Dev DB"
                    :subtitle context}))

  (local (name-label name) (label-entry :Name))
  (local (file-label _) (label-entry :File))
  (local file (gtk.Label {:visible true :label ""}))
  (local file-chooser (gtk.FileChooserButton {:visible true}))
  (local (category-label category) (label-entry :Category))  
  (local (author-label author) (label-entry :Author))
  (local (link-label link) (label-entry :Link))

  (local add (gtk.Button {:visible true :label "Add Resource"}))

  (fn add.on_clicked [self]
    (db:add-resource db data.name data.author data.licence data.link data.category
                     data.datatype data.data)
    (new.on_clicked self)
    )
  
  (fn file-chooser.on_file_set [self]
    (tset data :file (self:get_filename))
    (pp (self:get_filename)))
  
  (set scroll (gtk.Grid {:visible true
                         :column_spacing 10
                         :row_spacing 10
                         1 {1 file-label :top_attach 0 :left_attach 0}
                         2 {1 file :top_attach 0 :left_attach 2}
                         3 {1 file-chooser :top_attach 0 :left_attach 2}
                         4 {1 name-label :top_attach 1 :left_attach 0}
                         5 {1 name :top_attach 1 :left_attach 2}
                         6 {1 category-label :top_attach 2 :left_attach 0}
                         7 {1 category :top_attach 2 :left_attach 2}
                         8 {1 author-label :top_attach 3 :left_attach 0}
                         9 {1 author :top_attach 3 :left_attach 2}
                         10 {1 link-label :top_attach 4 :left_attach 0}
                         11 {1 link :top_attach 4 :left_attach 2}
                         12 {1 add :top_attach 5 :left_attach 2}
                         }))
  (container:pack_start scroll true true 0)
  (aw:set_titlebar titlebar))

(fn app.on_activate [self]
  (set aw self.active_window)
  (local titlebar (gtk.HeaderBar
                   {:visible true
                    :show_close_button true
                    :title "Indie Dev DB"
                    :subtitle context}))
  (display)
  (aw:add container)
  (aw:set_titlebar titlebar)
  (aw:present))

(local app-mt {:__index app})
(local gui-mt {:__index gui})

(fn gui.run [arg dbin]
  (set db dbin)  
  (app:run arg)
  app)


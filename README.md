# INDIE GAME DB

A database for storing game assets. Useful for when working with CC-BY and open source projects that require attribution.

## Running

``` bash
./lua_modules/bin/fennel main.fnl 
```

## Dependencies


GTK3+
GObject
sqlite3-dev
luajit

### lgi
`luarocks install --tree lua_modules lgi`

### lsqlite3
`luarocks install --tree lua_modules lsqlite3`

### Fennel
`luarocks install --tree lua_modules lsqlite3`

### luafilesystem
`luarocks install --tree lua_modules luafilesystem`
